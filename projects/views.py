from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, ConfirmForm
from tasks.forms import TaskForm
from projects.charts import task_gantt


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)

    assignees = []
    for task in project.tasks.all():
        if task.assignee not in assignees and task.assignee != project.owner:
            assignees.append(task.assignee)

    print(assignees)
    context = {
        "project": project,
        "assignees": assignees,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
        "name": "Create a new project",
        "button": "Create",
    }
    return render(request, "projects/form.html", context)


@login_required
def delete_item(request, type, id):
    if request.method == "POST":
        print("POST")
        print(type)
        if type == "task":
            task = Task.objects.filter(id=id)
            task.delete()
            return redirect("home")
        elif type == "project":
            project = Project.objects.filter(id=id)
            project.delete()
            return redirect("home")
    else:
        form = ConfirmForm()

    context = {
        "form": form,
        "type": type,
        "name": "Delete " + type,
        "button": "Confirm",
    }
    return render(request, "projects/form.html", context)


@login_required
def edit_item(request, type, id):
    if request.method == "POST":
        if type == "task":
            item = get_object_or_404(Task, id=id)
            form = TaskForm(request.POST, instance=item)
            if form.is_valid():
                form.save()
                return redirect("home")
        elif type == "project":
            item = get_object_or_404(Project, id=id)
            form = ProjectForm(request.POST, instance=item)
            if form.is_valid():
                form.save()
                return redirect("home")

    else:
        if type == "task":
            item = get_object_or_404(Task, id=id)
            form = TaskForm(instance=item)
        elif type == "project":
            item = get_object_or_404(Project, id=id)
            form = ProjectForm(instance=item)

    context = {
        "form": form,
        "type": type,
        "name": "Edit " + type,
        "button": "Done",
    }
    return render(request, "projects/form.html", context)


@login_required
def gantt_view(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    gantt_plot = task_gantt(tasks)

    assignees = []
    for task in project.tasks.all():
        if task.assignee not in assignees and task.assignee != project.owner:
            assignees.append(task.assignee)

    context = {
        'project': project,
        'gantt': gantt_plot,
        'assignees': assignees,
    }
    return render(request, "projects/gantt.html", context)


@login_required
def superuser_projects(request):
    if not request.user.is_superuser:
        return redirect("home")

    projects = Project.objects.all()
    context = {"projects": projects}
    return render(request, "projects/super_list.html", context)
