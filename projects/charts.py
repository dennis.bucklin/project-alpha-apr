import pandas as pd
from plotly.offline import plot
import plotly.express as px


def task_gantt(tasks):
    timeline_data = []
    for task in tasks:
        timeline_data.append(
            {
                'Task': f"{task.name} : {task.assignee.username} ",
                'Start': task.start_date,
                'Due': task.due_date,
                'Is Completed': task.is_completed,
            }
        )
    df = pd.DataFrame(timeline_data)
    fig = px.timeline(
        df,
        x_start="Start",
        x_end="Due",
        y="Task",
        color="Is Completed",
        template="plotly_dark",
    )
    fig.update_yaxes(autorange="reversed")
    gantt_plot = plot(fig, output_type="div")
    return gantt_plot
