from django.forms import ModelForm, Form, BooleanField
from projects.models import Project


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]


class ConfirmForm(Form):
    confirm = BooleanField()
