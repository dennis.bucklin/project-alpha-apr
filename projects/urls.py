from django.urls import path
from projects.views import (
    project_list,
    project_detail,
    create_project,
    delete_item,
    edit_item,
    gantt_view,
    superuser_projects,
)


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("delete/<str:type>/<int:id>/", delete_item, name="delete"),
    path("edit/<str:type>/<int:id>/", edit_item, name="edit"),
    path("<int:id>/gantt/", gantt_view, name="show_gantt"),
    path("manage/", superuser_projects, name="show_manage")
]
