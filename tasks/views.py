from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm, NoteForm
from projects.forms import ConfirmForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
        "name": "Create a new task",
        "button": "Create",
    }
    return render(request, "projects/form.html", context)


@login_required
def assigned_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)

    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)


@login_required
def note_list(request, id):
    task = get_object_or_404(Task, id=id)
    notes = task.notes.all()

    context = {
        'notes': notes,
        'name': task.name,
        'task': task,
    }
    return render(request, "tasks/notes.html", context)


@login_required
def create_note(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            note = form.save(False)
            note.task = task
            note.author = request.user
            note.save()
            return redirect("home")
    else:
        form = NoteForm()

    context = {
        'form': form,
        'name': "Add a note",
        'button': "Add",
    }
    return render(request, "projects/form.html", context)


@login_required
def complete_task(request, id):
    if request.method == "POST":
        task = get_object_or_404(Task, id=id)
        task.is_completed = not task.is_completed
        task.save()
        return redirect("show_project", task.project.id)
    else:
        form = ConfirmForm()

    context = {
        'form': form,
        'name': "Change status",
        'button': "Confirm",
    }
    return render(request, "projects/form.html", context)
