from django.urls import path
from tasks.views import (create_task,
                         assigned_tasks,
                         note_list,
                         create_note,
                         complete_task
                         )


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", assigned_tasks, name="show_my_tasks"),
    path("<int:id>/notes/", note_list, name="show_notes"),
    path("<int:id>/create/", create_note, name="create_note"),
    path("<int:id>/complete/", complete_task, name="complete_task"),
]
