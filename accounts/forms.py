from django.forms import Form, CharField, PasswordInput


class LoginForm(Form):
    username = CharField(max_length=150)
    password = CharField(
        max_length=150,
        widget=PasswordInput,
    )


class SignUpForm(Form):
    username = CharField(max_length=158)
    password = CharField(
        max_length=150,
        widget=PasswordInput,
    )
    password_confirmation = CharField(
        max_length=150,
        widget=PasswordInput,
    )
